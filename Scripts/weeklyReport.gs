/*
Weekly Reporting Helper: Dumps all GA Campaign data for the last week into a spreadsheet. Easy enough - doug.silkstone@glami.cz
*/

// Used to load the correct spreadsheet based on SHEET_URL
var SHEET_URL = 'https://docs.google.com/spreadsheets/d/1l2Bqsu_hOH8oKOyGDEKH3fcK9cS_d6SrjmMYFj9x_DI/edit#gid=0';
var SHEET_NAME = 'WEEKLY';

var useSheet = loadSpreadsheet(SHEET_URL, SHEET_NAME);

var accountList = []; // Holds GA Account Data`
var countryList = [
  //  { name: 'CZ', longName: 'BAU Czech Republic', shortName: 'glami.cz ALL', propertyId: '76214327', dataset: [] },
    { name: 'SK', longName: 'BAU Slovakia', shortName: 'glami.sk ALL', propertyId: '129619770', dataset: [] },
    { name: 'RO', longName: 'BAU Romania', shortName: 'glami.ro ALL', propertyId: '149473193', dataset: [] },
    { name: 'HU', longName: 'BAU Hungary', shortName: 'glami.hu ALL', propertyId: '159249074', dataset: [] },
    { name: 'TR', longName: 'BAU Turkey', shortName: 'glami.com.hr ALL', propertyId: '171616401', dataset: [] },
    { name: 'RU', longName: 'BAU Russia', shortName: 'glami.ru ALL', propertyId: '171618759', dataset: [] },
    { name: 'GR', longName: 'BAU Greece', shortName: 'glami.gr ALL', propertyId: '171624390', dataset: [] },
    { name: 'BG', longName: 'BAU Bulgaria', shortName: 'glami.bg ALL', propertyId: '184822567', dataset: [] },
    { name: 'HR', longName: 'BAU Croatia', shortName: 'glami.hr ALL', propertyId: '184882373', dataset: [] },
    { name: 'SI', longName: 'BAU Slovenia', shortName: 'glami.si ALL', propertyId: '184969812', dataset: [] },
]

var dumpList = [] // Holds account agnostic campaign structure. Needed to make spreadsheet update faster

var d = new Date();
d.setDate(d.getDate() - (d.getDay() + 6) % 7);
d.setDate(d.getDate() - 7);
var lastMonday = d;
var lastSunday = new Date(d.getFullYear(), d.getMonth(), d.getDate() + 6);

var reportData = {
    Headers: [
        'Campaign',
        'Account',
        'Cost',
        'Clicks',
        'Impressions',
        'CPC',
        'Revenue',
        'Sessions',
        'Transcations',
        'AOV',
        'Shop GMV'
    ]
};


function main() {
    
    useSheet.getRange(1, 1, 35000, reportData.Headers.length).clearContent()
    dumpList.push(reportData.Headers)

    for (i in countryList) {
        var listCountry = countryList[i];
        var fetchData = createReport(listCountry.propertyId)

        // If we have actual campaign data in account (not just the (not set) default catch-all) then we'll process that data. If not, skip.
        if (fetchData.rows && fetchData.rows.length > 1) {
            Logger.log('%s - Campaigns: %s', countryList[i].name, fetchData.rows.length - 1)

            countryList[i].dataset = fetchData.rows;
            countryList[i].dataset.shift(); // Remove first entry

            // check the profileInfo.profileName variable against the countryList.shortName and swap out accordingly.
            var replaceMe;
            for (z in countryList) {
                if (countryList[z].shortName.toLowerCase() == fetchData.profileInfo.profileName.toLowerCase()) {
                    replaceMe = countryList[i].longName;
                }
            }
            for (x in listCountry.dataset) {
                listCountry.dataset[x].splice(1, 0, replaceMe); // Add 
                dumpList.push(listCountry.dataset[x]);
            }
        }
    }
    // Now to dump each set of stats into each countries tab
    Logger.log(dumpList)

    var range = useSheet.getRange(1, 1, dumpList.length, reportData.Headers.length);
    range.setValues(dumpList);

    updateInfoSheet()
}

function updateInfoSheet() {

    var infoSheet = loadSpreadsheet(SHEET_URL, 'INFO');
    infoSheet.getRange('A1').setValue('WEEKLY SHEET DATA:');
    infoSheet.getRange('B1').setValue(lastMonday.getDate() + '/' + (parseInt(lastMonday.getMonth()) + 1) + ' - ' + lastSunday.getDate() + '/' + (parseInt(lastSunday.getMonth()) + 1));
    Logger.log('Report Complete - Check %s', SHEET_URL)



}

function createReport(propertyId) {
    //.. build report query..
    var profileId = propertyId;
    var results = Analytics.Data.Ga.get(
        'ga:' + profileId,
        formatDate(lastMonday), // Start date in yyyy-mm-dd format.
        formatDate(lastSunday), // End date in yyyy-mm-dd format.
        'ga:adCost,ga:adClicks,ga:impressions,ga:CPC,ga:transactionRevenue,ga:sessions,ga:transactions,ga:revenuePerTransaction,ga:metric1', // List of all metrics to retrieve.
        {
            // Filter for Firefox browser users in the USA. See
            // https://developers.google.com/analytics/devguides/reporting/core/v3/reference#filters
            // for filter syntax, and
            // https://developers.google.com/analytics/devguides/reporting/core/dimsmets
            // for the list of supported Dimensions and Metrics.
            'max-results': 250000,
            'dimensions': 'ga:campaign',
            'filters': 'ga:sourceMedium==google / cpc,ga:sourceMedium==Sklik / cpc'
        }
    );

    /*    var metricCount = 10;

        Logger.log('View (Profile) Name: %s', results.profileInfo.profileName);
        for (x in results.rows[1]) {
            Logger.log('Metric: %s', results.rows[1][x]);
        }
    */
    return results;

}

/*a
SearchImpressionShare
SearchImpressionSharea
*/


function formatDate(date) {
    //Formats the date into YYYY-MM-DD for GA reporting..
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
}


function listAccounts() {
    var accounts = Analytics.Management.Accounts.list();
    if (accounts.items && accounts.items.length) {
        for (var i = 0; i < accounts.items.length; i++) {
            var account = accounts.items[i];
            // Logger.log('Account: name "%s", id "%s".', account.name, account.id);

            var obj = {
                Account: {
                    name: account.name,
                    id: account.id
                }
            }

            accountList.push(obj)

            // List web properties in the account.
            listWebProperties(account.id, i);
        }
    } else {
        Logger.log('No accounts found.');
    }

    // Log EVERYTHING at end
    Logger.log(accountList)
    for (x in accountList) {
        for (var i = accountList[x].Account.webProperty.Profile.length - 1; i >= 0; i--) {
            Logger.log("%s,%s\t%s,%s\t\t%s,%s", accountList[x].Account.name, accountList[x].Account.id, accountList[x].Account.webProperty.name, accountList[x].Account.webProperty.id, accountList[x].Account.webProperty.Profile[i].name, accountList[x].Account.webProperty.Profile[i].id)
        }
    }

    if (accountList > 0) {
        return true;
    } else {
        return false;
    }
}

/**
 * Lists web properites for an Analytics account.
 * @param  {string} accountId The account ID.
 */
function listWebProperties(accountId, x) {
    var webProperties = Analytics.Management.Webproperties.list(accountId);
    if (webProperties.items && webProperties.items.length) {
        for (var i = 0; i < webProperties.items.length; i++) {
            var webProperty = webProperties.items[i];
            // Logger.log('\tWeb Property: name "%s", id "%s".', webProperty.name, webProperty.id);

            var obj = {
                name: webProperty.name,
                id: webProperty.id,
                Profile: []
            }

            accountList[x].Account['webProperty'] = obj;


            // List profiles in the web property.
            listProfiles(accountId, webProperty.id, x, i);
        }

    } else {
        Logger.log('\tNo web properties found.');
    }
}

/**
 * Logs a list of Analytics accounts profiles.
 * @param  {string} accountId     The Analytics account ID
 * @param  {string} webPropertyId The web property ID
 */
function listProfiles(accountId, webPropertyId, accountListInt, webPropertyInt) {
    // Note: If you experience "Quota Error: User Rate Limit Exceeded" errors
    // due to the number of accounts or profiles you have, you may be able to
    // avoid it by adding a Utilities.sleep(1000) statement here.

    var profiles = Analytics.Management.Profiles.list(accountId,
        webPropertyId);
    if (profiles.items && profiles.items.length) {
        for (var i = 0; i < profiles.items.length; i++) {
            var profile = profiles.items[i];
            // Logger.log('\t\tProfile: name "%s", id "%s".', profile.name,profile.id);

            var obj = {
                name: profile.name,
                id: profile.id
            }

            accountList[accountListInt].Account.webProperty.Profile.push(obj)


        }
    } else {
        Logger.log('\t\tNo web properties found.');
    }
}


function loadSpreadsheet(sheetUrl, sheetName) {

    var sheetNames = [];
    var ss = SpreadsheetApp.openByUrl(sheetUrl);

    var noSheets = ss.getNumSheets()
    var sheetStack = ss.getSheets()

    // Dump sheet names to sheetNames array ..
    for (x in sheetStack) {
        Logger.log(x + '-' + sheetStack[x].getName())
        sheetNames.push(sheetStack[x].getName())
    }

    // .. then check through the original sheetName var to see if it exists..

    if (sheetNames.indexOf(sheetName) >= 0) {
        Logger.log("found sheet name - continuing..")
        var sheet = ss.getSheetByName(sheetName);
        return sheet;

    } else {
        Logger.log("Not found, check your tab name.")
    }


}